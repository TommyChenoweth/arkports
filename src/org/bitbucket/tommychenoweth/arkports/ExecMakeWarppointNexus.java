package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecMakeWarppointNexus extends ArkPortsCommandExecutor{
	
	public ExecMakeWarppointNexus(WarppointRepository warppoint_repo)
	{
		super(warppoint_repo);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 1)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		
		Warppoint nexus = null;
		
		try
		{
			nexus = GetWarppointRepo().GetAndRemoveDanglingWarppointByName(args[0]);
			if(nexus == null)
			{
				return false;
			}
			
			WarppointNexus wp_nexus = new WarppointNexus(nexus);
			GetWarppointRepo().AddWarppointConnection(wp_nexus);
			sender.sendMessage(args[0] + " is now a nexus.");
		}
		catch(Exception e)
		{
			if(nexus != null)
				GetWarppointRepo().AddDanglingWarppoint(nexus);
			
			sender.sendMessage(e.getMessage());
			return false;
		}
		
		return true;
    }

}

package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecMakeWarppointPortal extends ArkPortsCommandExecutor {
	
	ExecMakeWarppointPortal(WarppointRepository warppoint_repo)
	{
		super(warppoint_repo);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 2)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		if(args[0] == args[1])
		{
			sender.sendMessage("You can't connect a warppoint to itself.");
			return false;
		}
		
		Warppoint wp_first = null;
		Warppoint wp_second = null;
		
		try
		{
			wp_first = GetWarppointRepo().GetAndRemoveDanglingWarppointByName(args[0]);
			if(wp_first == null)
			{
				sender.sendMessage("Failed to find a dangling warppoint named " + args[0] + ".");
				return false;
			}
			
			wp_second = GetWarppointRepo().GetAndRemoveDanglingWarppointByName(args[1]);
			if(wp_second == null)
			{
				GetWarppointRepo().AddDanglingWarppoint(wp_first);
				sender.sendMessage("Failed to find a dangling warppoint named " + args[1] + ".");
				return false;
			}
			
			WarppointPortal wp_portal = new WarppointPortal(wp_first, wp_second);
			GetWarppointRepo().AddWarppointConnection(wp_portal);
			sender.sendMessage(args[0] + " and " + args[1] + " now form a portal.");
		}
		catch(Exception e)
		{
			if(wp_first != null)
				GetWarppointRepo().AddDanglingWarppoint(wp_first);
			if(wp_second != null)
				GetWarppointRepo().AddDanglingWarppoint(wp_second);
			
			sender.sendMessage(e.getMessage());
			
			return false;
		}
		
		return true;
    }

}

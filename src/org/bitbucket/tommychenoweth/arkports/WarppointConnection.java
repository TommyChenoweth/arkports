package org.bitbucket.tommychenoweth.arkports;

import java.io.Serializable;
import java.util.ArrayList;

import org.bukkit.Location;


/**
 * Represents Warppoints that are connected to each other. 
 * 
 * @author PyroKinesis
 */
public abstract class WarppointConnection implements Serializable
{
	public static final long serialVersionUID = 6465664418687L;
	
	/**
	 * Connect a Warppoint to this WarppointConnection.
	 * @param warppoint The Warppoint to connect to the WarppointConnection.
	 * @throws UnsupportedOperationException Thrown when this function isn't supported by the
	 * WarppointConnection.
	 */
	public abstract void ConnectWarppoint(Warppoint warppoint) throws UnsupportedOperationException;
	
	/**
	 * Disconnect a Warppoint from this WarppointConnection.
	 * @param warppoint The Warppoint to disconnect from this WarppointConnection.
	 * @throws IllegalArgumentException Thrown when the WarppointConnection doesn't contain the
	 * passed Warppoint.
	 */
	public abstract void DisconnectWarppoint(Warppoint warppoint) throws IllegalArgumentException;
	
	/**
	 * Get a list of all Warppoints within this WarppointConnection.
	 * @return The list of this WarppointConnection's Warppoints.
	 */
	public abstract ArrayList<Warppoint> GetAllWarppoints();

	/**
	 * If the passed position is at some Warppoint WP, return the the Warppoint that
	 * WP is connected to, or return null if the passed position isn't at any Warppoint.
	 * @param location Some point in 3-space.
	 * @return Either a destination or null depending on the position.
	 */
	public abstract Warppoint GetDestinationForLocation(final Location location);
	
	/** 
	 * This function will return the Warppoint of the passed name if it exists; otherwise, 
	 * it will return null.
	 * @param name A string containing the naming the desired Warppoint.
	 */
	public abstract Warppoint GetWarppointByName(final String name);
	
	/**
	 * Returns true if this WarppointConnection contains a Warppoint by the passed name.
	 * @param name The name the Warppoints should be compared to.
	 */
	public abstract boolean NameCollisionWithWarppoints(final String name);
	
	/**
	 * Determines whether or not this WarppointConnection is in a valid state.
	 * @return If the WarppointConnection is in a valid state, the function returns true;
	 * otherwise, the function returns false.
	 */
	public abstract boolean IsValid();
	
	public abstract String toString();
}

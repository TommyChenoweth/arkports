package org.bitbucket.tommychenoweth.arkports;

import java.util.LinkedList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecListAllCommands extends ArkPortsCommandExecutor
{
	private String _help_message = new String("Commands: ");

	public ExecListAllCommands(WarppointRepository warppoint_repo, LinkedList<String> all_commands) 
	{
		super(warppoint_repo);
		for(String cmd : all_commands)
		{
			_help_message += (cmd + " ");
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		sender.sendMessage(_help_message);
		
		return true;
    }
}

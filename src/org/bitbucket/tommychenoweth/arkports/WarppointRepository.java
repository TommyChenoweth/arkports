package org.bitbucket.tommychenoweth.arkports;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.IllegalArgumentException;

import java.util.ArrayList;
import java.util.LinkedList;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
/**
 * Maintains Warppoints and WarppointConnections.
 * <br><br>
 * It ensures all of its Warppoints have unique names, and provides functions to easily address all of its Warppoints.
 * @author PyroKinesis
 *
 */
public class WarppointRepository {

	private final String _CONNECTED_WARPPOINTS_FILENAME = "connected_warppoints";
	private final String _DANGLING_WARPPOINTS_FILENAME = "dangling_warppoints";
	
	private ArrayList<Warppoint> _dangling_warppoints = new ArrayList<Warppoint>();
	private ArrayList<WarppointConnection> _connected_warppoints = new ArrayList<WarppointConnection>();
	
	/**
	 * Players on this list are considered locked. Locked players cannot make use of Warppoints.
	 */
	private LinkedList<Player> _locked_players = new LinkedList<Player>();
	
	/**
	 * Attempts to connect a Warppoint to a WarppointConnection.
	 * @param warppoint_name The name of a dangling Warppoint.
	 * @param connection_name The name of a Warppoint that is part of a WarppointConnection
	 * @throws IllegalArgumentException Thrown when the first parameter isn't the name of a dangling Warppoint, or the second parameter isn't the name of a Warppoint that's part of a WarppointConnection.
	 * @throws UnsupportedOperationException Thrown when the WarppointConnection can't/won't connect the dangling Warppoint.
	 */
	private void _ConnectWarppointToConnection(String warppoint_name, String connection_name) throws IllegalArgumentException, UnsupportedOperationException
	{
		WarppointConnection connection = null;
		connection = _GetConnectionByWarppointName(connection_name);
		if(connection == null)
			throw new IllegalArgumentException(connection_name + " isn't a connection.");
		
		Warppoint wp = null;
		wp = _GetDanglingWarppointByName(warppoint_name);
		if(wp == null)
			throw new IllegalArgumentException(warppoint_name + " isn't a dangling warppoint.");
		
		connection.ConnectWarppoint(wp);
		_dangling_warppoints.remove(wp);
	}

	private <T> ArrayList<T> _DeserializeArrayListFromFile(String filename) throws IOException, ClassNotFoundException
	{
		FileInputStream file_is = new FileInputStream(filename);
		ObjectInputStream obj_is = new ObjectInputStream(file_is);
		
		@SuppressWarnings("unchecked")
		ArrayList<T> al = (ArrayList<T>) obj_is.readObject();
		
		obj_is.close();
		file_is.close();
		
		return al;
	}

	/**
	 * Returns the WarppointConnection that contains the Warppoint with the passed name. 
	 * @param warppoint_name The name of the Warppoint to search for.
	 * @return Returns a WarppointConnection object containing the Warppoint with the passed name, or 
	 * returns null if the Warppoint with the passed name isn't part of a WarppointConnection.
	 */
	private WarppointConnection _GetConnectionByWarppointName(String warppoint_name)
	{
		for(WarppointConnection connection : _connected_warppoints)
		{
			if(connection.NameCollisionWithWarppoints(warppoint_name))
				return connection;
		}
		
		return null;
	}

	/**
	 * Returns the dangling Warppoint with the passed name.
	 * @param warppoint_name The name of the Warppoint to search for.
	 * @return Returns a Warppoint object with the passed name if it's found; otherwise, it returns null.
	 */
	private Warppoint _GetDanglingWarppointByName(final String warppoint_name)
	{
		for(Warppoint wp : _dangling_warppoints)
		{
			if(warppoint_name.equalsIgnoreCase(wp.GetName()))
				return wp;
		}
		
		return null;
	}

	/**
	 * If the passed Location is at a connected Warppoint, the function returns the destination Warppoint 
	 * of that Warppoint.
	 * @param location The location to consider.
	 * @return A Warppoint object or null depending on whether or not the passed Location is at a connected
	 * Warppoint.
	 */
	private Warppoint _GetDestinationForPosition(final Location location)
	{
		for(WarppointConnection connection : _connected_warppoints)
		{
			Warppoint destination = null;
			destination = connection.GetDestinationForLocation(location);
			if(destination != null)
				return destination;
		}
		
		return null;
	}
	
	private boolean _IsPlayerLocked(Player player)
	{
		return (_locked_players.indexOf(player) != -1);
	}
	
	private void _LockPlayer(Player player)
	{
		_locked_players.add(player);
	}
	
	private <T> void _SerializeArrayListToFile(ArrayList<T> al, String filename) throws FileNotFoundException, IOException
	{
		FileOutputStream file_os = new FileOutputStream(filename);
		ObjectOutputStream obj_os = new ObjectOutputStream(file_os);
		obj_os.writeObject(al);
		obj_os.close();
		file_os.close();
	}

	private void _UnlockPlayer(Player player)
	{
		_locked_players.remove(player);
	}
	
	/**
	 * Determines whether or not the passed name is in use by a Warppoint.
	 * @param warppoint_name The name to consider
	 * @return True if the passed name is in use by a Warppoint; otherwise, false.
	 */
	private boolean _WarppointNameCollision(String warppoint_name)
	{
		for(WarppointConnection connection : _connected_warppoints)
		{
			if(connection.NameCollisionWithWarppoints(warppoint_name))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Attempts to add a Warppoint to the list of dangling Warppoints.
	 * @param warppoint The Warppoint that should be added.
	 * @throws IllegalArgumentException Thrown when a name collision is encountered.
	 */
	public void AddDanglingWarppoint(Warppoint warppoint) throws IllegalArgumentException
	{
		if(_WarppointNameCollision(warppoint.GetName()))
			throw new IllegalArgumentException("A Warppoint named " + warppoint.GetName() + " already exists.");
		
		_dangling_warppoints.add(warppoint);
	}

	/**
	 * Attempts to create a Warppoint using the passed name and location, and add it to the list of dangling Warppoints.
	 * @param warppoint_name The new Warppoint's name.
	 * @param warppoint_location The new Warppoint's location.
	 * @throws IllegalArgumentException Thrown when a name collision is encountered.
	 */
	public void AddDanglingWarppoint(String warppoint_name, Location warppoint_location) throws IllegalArgumentException
	{
		if(_WarppointNameCollision(warppoint_name))
			throw new IllegalArgumentException("A Warppoint named " + warppoint_name + " already exists.");
		
		_dangling_warppoints.add(new Warppoint(warppoint_name, warppoint_location));
	}
	
	/**
	 * Attempts to add a WarppointConnection to the list of WarppointConnections.
	 * @param connection The connection that should be added.
	 * @throws IllegalArgumentException Thrown when a name collision is encountered between any of the
	 * passed WarppointConnection's Warppoints and any of the Warppoints - connected or otherwise - already
	 * contained within this WarppointRepository.
	 */
	public void AddWarppointConnection(WarppointConnection connection) throws IllegalArgumentException
	{
		// Check for name collisions
		ArrayList<Warppoint> wps = connection.GetAllWarppoints();
		for(Warppoint wp : wps)
		{
			if(_WarppointNameCollision(wp.GetName()))
				throw new IllegalArgumentException("A WarppointConnection named " + wp.GetName() + " already exists.");
		}
		
		_connected_warppoints.add(connection);
	}

	/**
	 * Attempts to connect a dangling Warppoint to a connected Warppoint's WarppointConnection. 
	 * One of the two parameters must be a dangling Warppoint, and one must be a connected Warppoint; however,
	 * the order of the names is not important.
	 * @param first_wp_name The name of a dangling Warppoint or a connected Warppoint.
	 * @param second_wp_name The name of a dangling Warppoint or a connected Warppoint.
	 * @throws IllegalArgumentException Thrown when the parameters don't correspond to a dangling Warppoint
	 * and a connected Warppoint. This can be thrown when one name or the other doesn't correspond to a 
	 * Warppoint, or when both names correspond to a Warppoint of the same type (connected or dangling).
	 * @throws NoSuchMethodException Thrown when the WarppointConnection refuses to connect the dangling Warppoint.
	 */
	public void ConnectWarppoints(String first_wp_name, String second_wp_name) throws IllegalArgumentException, NoSuchMethodException
	{
		try
		{
			_ConnectWarppointToConnection(first_wp_name, second_wp_name);
			return;
		}
		catch(UnsupportedOperationException e)
		{
			throw e;
		}
		catch(IllegalArgumentException e)
		{
		}
		
		try
		{
			_ConnectWarppointToConnection(second_wp_name, first_wp_name);
			return;
		}
		catch(UnsupportedOperationException e)
		{
			throw e;
		}
		catch(IllegalArgumentException e)
		{
		}
		
		throw new IllegalArgumentException("Failed to find a WarppointConnection or Warppoint.");
	}

	/**
	 * Attempts to disconnect a Warppoint from a WarppointConnection. If WarppointConnection isn't
	 * in a valid state after the disconnection, the WarppointConnection is destroyed, and its
	 * Warppoints are added back into the list of dangling Warppoints.
	 * @param warppoint_name The Warppoint name to search for.
	 * @throws IllegalArgumentException Thrown when the passed name doesn't correspond to a
	 * connected Warppoint.
	 */
	public void DisconnectWarppoint(String warppoint_name) throws IllegalArgumentException
	{
		WarppointConnection connection = null;
		connection = _GetConnectionByWarppointName(warppoint_name);
		if(connection == null)
			throw new IllegalArgumentException(warppoint_name + " isn't in a WarppointConnection.");
		
		Warppoint wp = null;
		wp = connection.GetWarppointByName(warppoint_name);
		if(wp == null)
			throw new IllegalArgumentException(warppoint_name + " isn't in a WarppointConnection.");
		
		connection.DisconnectWarppoint(wp);
		_dangling_warppoints.add(wp);
		if(connection.IsValid())
			return;
		
		ArrayList<Warppoint> orphaned_wps = connection.GetAllWarppoints();
		_connected_warppoints.remove(connection);
		_dangling_warppoints.addAll(orphaned_wps);
	}

	/**
	 * Attempts to remove the Warppoint with the passed name from the list of dangling Warppoints, and
	 * return it.
	 * @param warppoint_name The name to search for.
	 * @return If a dangling Warppoint with the passed name was found, the function returns it; otherwise,
	 * the function returns null.
	 */
	public Warppoint GetAndRemoveDanglingWarppointByName(String warppoint_name)
	{
		Warppoint wp = null;
		wp = _GetDanglingWarppointByName(warppoint_name);
		if(wp == null)
			return null;
		
		_dangling_warppoints.remove(wp);
		return wp;
	}

	/**
	 * Gets the destination Warppoint for the player if he's standing at a connected Warppoint.
	 * @param player The player to consider.
	 * @return If the player is standing at a connected Warppoint, the function returns that Warppoint's
	 * destination Warppoint; otherwise, the function returns null.
	 */
	public Warppoint GetDestinationForPlayer(Player player)
	{
		Warppoint warppoint = _GetDestinationForPosition(player.getLocation());
		
		if(warppoint == null)
		{
			_UnlockPlayer(player);
			return null;
		}
		if(_IsPlayerLocked(player))
			return null;
		
		_LockPlayer(player);
		
		return warppoint;
	}

	/**
	 * Prints out every Warppoint within this WarppointRepository to the sender.
	 * @param sender The sender who should see the Warppoints.
	 */
	public void ListWarppointsToSender(CommandSender sender)
	{
		sender.sendMessage("=====================\n" +
				           "|| Connected Warppoints ||\n" +
				           "=====================\n");
		if(_connected_warppoints.isEmpty())
			sender.sendMessage("*NONE*\n");
		for(WarppointConnection connection : _connected_warppoints)
		{
			sender.sendMessage(connection + "\n");
		}
		
		sender.sendMessage("======================\n" +
			                "||  Dangling Warppoints  ||\n" +
			                "=====================\n");
		if(_dangling_warppoints.isEmpty())
			sender.sendMessage("*NONE*\n");
		for(Warppoint wp : _dangling_warppoints)
		{
			sender.sendMessage(wp.GetName() + "\n");
		}
	}

	public void LoadWarppoints(String directory) throws IOException, ClassNotFoundException
	{
		// TODO Remove code duplication.
		ArrayList<WarppointConnection> des_connections = null;
		des_connections = _DeserializeArrayListFromFile(directory + _CONNECTED_WARPPOINTS_FILENAME);
		if(des_connections != null)
			_connected_warppoints = des_connections;
		
		ArrayList<Warppoint> des_warppoints = null;
		des_warppoints = _DeserializeArrayListFromFile(directory + _DANGLING_WARPPOINTS_FILENAME);
		if(des_warppoints != null)
			_dangling_warppoints = des_warppoints;
	}

	public void SaveWarppoints(String directory) throws FileNotFoundException, IOException
	{
		_SerializeArrayListToFile(_connected_warppoints, directory + _CONNECTED_WARPPOINTS_FILENAME);
		_SerializeArrayListToFile(_dangling_warppoints, directory + _DANGLING_WARPPOINTS_FILENAME);
	}

}

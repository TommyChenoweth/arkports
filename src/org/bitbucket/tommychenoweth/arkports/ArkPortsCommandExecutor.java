package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.CommandExecutor;

public abstract class ArkPortsCommandExecutor implements CommandExecutor{

	private WarppointRepository _warppoint_repo;
	
	public ArkPortsCommandExecutor(WarppointRepository warppoint_repo)
	{
		_warppoint_repo = warppoint_repo;
	}
	
	public WarppointRepository GetWarppointRepo()
	{
		return _warppoint_repo;
	}
}

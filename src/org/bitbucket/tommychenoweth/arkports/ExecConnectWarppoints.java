package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecConnectWarppoints extends ArkPortsCommandExecutor {

	public ExecConnectWarppoints(WarppointRepository warppoint_repo) 
	{
		super(warppoint_repo);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 2)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		if(args[0] == args[1])
		{
			sender.sendMessage("You can't connect a warppoint to itself.");
			return false;
		}
		
		try
		{
			GetWarppointRepo().ConnectWarppoints(args[0], args[1]);
			sender.sendMessage(args[0] + " and " + args[1] + " were successfully connected.");
		}
		catch(Exception e)
		{
			sender.sendMessage(e.getMessage());
			
			return false;
		}
		
		return true;
    }

}

package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecDeleteWarppoint extends ArkPortsCommandExecutor {

	public ExecDeleteWarppoint(WarppointRepository warppoint_repo) 
	{
		super(warppoint_repo);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 1)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		
		if(GetWarppointRepo().GetAndRemoveDanglingWarppointByName(args[0]) != null)
			sender.sendMessage(args[0] + " was deleted.");
		else
			sender.sendMessage(args[0] + " was not found.");
		
		return true;
    }

}

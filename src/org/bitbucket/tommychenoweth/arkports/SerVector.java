package org.bitbucket.tommychenoweth.arkports;

import java.io.Serializable;

import org.bukkit.util.Vector;

public class SerVector implements Serializable{
	
	public static final long serialVersionUID = 554673435L;
	
	private double _x;
	private double _y;
	private double _z;
	
	public SerVector(Vector vector)
	{
		_x = vector.getX();
		_y = vector.getY();
		_z = vector.getZ();
	}
	
	public Vector ToVector()
	{
		return new Vector(_x, _y, _z);
	}

}

package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {
	
	private WarppointRepository _warppoint_repo;
	
	public PlayerMoveListener(WarppointRepository warppoint_repo)
	{
		_warppoint_repo = warppoint_repo;
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		Warppoint wp = _warppoint_repo.GetDestinationForPlayer(player);
		if(wp == null)
			return;
		
		try
		{
			player.teleport(wp.GetPlayerWarpLocation());
		}
		catch(NullPointerException e)
		{
			player.sendMessage(e.getMessage());
		}
	}

}

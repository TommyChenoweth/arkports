package org.bitbucket.tommychenoweth.arkports;

import java.io.Serializable;

import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class Warppoint implements Serializable 
{
	
	public static final long serialVersionUID = 864564566L;
	
	public static Server _server = null;
	
	private String _name;
	
	private String _world_name;
	
	// This is where the player will be placed if he warps to this Warppoint.
	private SerLocation _player_warp_location;
	
	// The origin and radius form a sphere for collision detection with the player.
	private SerVector _origin;
	private static final double _RADIUS = 0.5f;
	
	Warppoint(final String name, final Location location)
	{
		_name = name.toLowerCase();
		_world_name = location.getWorld().getName();
		_player_warp_location = new SerLocation(location);
		_origin = new SerVector(location.toVector());
	}
	
	public static void SetServer(Server server)
	{
		_server = server;
	}
	
	public String GetName()
	{
		return _name;
	}
	
	public Location GetPlayerWarpLocation()
	{
		World world = null;
		world = _server.getWorld(_world_name);
		if(world == null)
			throw new NullPointerException("Unable to find a world named " + _world_name);
		
		return _player_warp_location.ToLocation(world);
	}
	
	public boolean LocationIsAtWarppoint(Location location) throws NullPointerException
	{
		Vector position = location.toVector();
		if(!position.isInSphere(_origin.ToVector(), _RADIUS))
				return false;
		
		if(!location.getWorld().getName().equalsIgnoreCase(_world_name))
			return false;
		
		return true;
	}

}

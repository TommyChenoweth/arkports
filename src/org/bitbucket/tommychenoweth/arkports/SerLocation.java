package org.bitbucket.tommychenoweth.arkports;

import java.io.Serializable;

import org.bukkit.Location;
import org.bukkit.World;

public class SerLocation implements Serializable {
	
	private double _x;
	private double _y;
	private double _z;
	private float _pitch;
	private float _yaw;
	
	public final static long serialVersionUID = 1673524654L;
	
	public SerLocation(Location location)
	{
		_x = location.getX();
		_y = location.getY();
		_z = location.getZ();
		_pitch = location.getPitch();
		_yaw = location.getYaw();
	}
	
	public SerLocation()
	{
		this(new Location(null, 0.0d, 0.0d, 0.0d, 0.0f, 0.0f));
	}
	
	public Location ToLocation(World world)
	{
		return new Location(world, _x, _y, _z, _yaw, _pitch);
	}

}

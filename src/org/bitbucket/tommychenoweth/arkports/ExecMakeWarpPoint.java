package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ExecMakeWarppoint extends ArkPortsCommandExecutor{
	
	public ExecMakeWarppoint(WarppointRepository warppoint_repo)
	{
		super(warppoint_repo);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 1)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		if(!(sender instanceof Player))
		{
			sender.sendMessage("This command cannot be used from the console.");
			return false;
		}
		
		Player player = (Player)sender;
		
		try
		{
			GetWarppointRepo().AddDanglingWarppoint(args[0], player.getLocation());
			sender.sendMessage("You've created a warppoint named " + args[0]);
		}
		catch(IllegalArgumentException e)
		{
			player.sendMessage(e.getMessage());
			return false;
		}
		
		return true;
    }

}

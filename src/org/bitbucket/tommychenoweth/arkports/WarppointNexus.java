package org.bitbucket.tommychenoweth.arkports;

import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Location;


/**
 * Represents one or more gateway Warppoints connected to a common Warppoint nexus for one-way travel.
 * @author Thomas Chenoweth
 */
public class WarppointNexus extends WarppointConnection
{
	
	public static final long serialVersionUID = 5464987643673L;
	
	// This is the Warppoint that the gateways will place the player at if he moves into them.
	private Warppoint _warppoint_nexus;
	
	/** 
	 * These are the Warppoints that will place the player at the nexus.
	 */
	private ArrayList<Warppoint> _nexus_gateways = new ArrayList<Warppoint>();
	
	public WarppointNexus(Warppoint nexus)
	{
		_warppoint_nexus = nexus;
	}
	
	@Override
	public void ConnectWarppoint(Warppoint warppoint)
	{
		_nexus_gateways.add(warppoint);
	}
	
	@Override
	public void DisconnectWarppoint(Warppoint warppoint) throws IllegalArgumentException
	{
		if(warppoint == _warppoint_nexus)
		{
			_warppoint_nexus = null;
			return;
		}
		
		Iterator<Warppoint> wp_it = _nexus_gateways.iterator();
		while(wp_it.hasNext())
		{
			Warppoint wp = wp_it.next();
			if(wp == warppoint)
			{
				wp_it.remove();
				
				return;
			}
		}
		
		throw new IllegalArgumentException(warppoint.GetName() + " not found in " + _warppoint_nexus.GetName());
	}
	
	@Override
	public ArrayList<Warppoint> GetAllWarppoints()
	{
		ArrayList<Warppoint> all_wps = new ArrayList<Warppoint>(_nexus_gateways);
		
		if(_warppoint_nexus != null)
			all_wps.add(_warppoint_nexus);
		
		return all_wps;
	}
	
	@Override
	public Warppoint GetDestinationForLocation(final Location location)
	{
		for(Warppoint wp : _nexus_gateways)
		{
			if(wp.LocationIsAtWarppoint(location))
			{
				return _warppoint_nexus;
			}
		}
		
		return null;
	}
	
	@Override
	public Warppoint GetWarppointByName(final String name)
	{
		if(name.equalsIgnoreCase(_warppoint_nexus.GetName()))
			return _warppoint_nexus;
		
		for(Warppoint wp : _nexus_gateways)
		{
			if(name.equalsIgnoreCase(wp.GetName()))
					return wp;
		}
		
		return null;
	}
	
	@Override
	public boolean IsValid()
	{
		return (_warppoint_nexus != null);
	}

	@Override
	public boolean NameCollisionWithWarppoints(final String name)
	{
		if(name.equalsIgnoreCase(_warppoint_nexus.GetName()))
			return true;
		
		for(Warppoint wp : _nexus_gateways)
		{
			if(name.equalsIgnoreCase(wp.GetName()))
				return true;
		}
		
		return false;
	}
	
	@Override
	public  String toString()
	{
		String out = _warppoint_nexus.GetName() + " [";
		for(Warppoint wp : _nexus_gateways)
		{
			out += (" " + wp.GetName() + ";");
		}
		out += " ]";
		
		return out;
	}
}

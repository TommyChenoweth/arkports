package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecListWarppoints extends ArkPortsCommandExecutor {

	ExecListWarppoints(WarppointRepository warppoint_repo)
	{
		super(warppoint_repo);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 0)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		
		GetWarppointRepo().ListWarppointsToSender(sender);
		return true;
    }
	
}

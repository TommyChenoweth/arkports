package org.bitbucket.tommychenoweth.arkports;

import java.util.ArrayList;

import org.bukkit.Location;

/**
 * Represents two Warppoints connected to each other for two-way travel.
 * @author PyroKinesis
 *
 */
public class WarppointPortal extends WarppointConnection
{

	public static final long serialVersionUID = 446867634L;
	
	private boolean _is_valid = true;
	
	// This Portal links the following two warppoints.
	private Warppoint _first_warppoint;
	private Warppoint _second_warppoint;
	
	public WarppointPortal(Warppoint first_warppoint, Warppoint second_warppoint)
	{
		_first_warppoint = first_warppoint;
		_second_warppoint = second_warppoint;
	}
	
	/**
	 * Currently, this function does nothing.
	 */
	@Override
	public void ConnectWarppoint(Warppoint warppoint) throws UnsupportedOperationException
	{
		throw new UnsupportedOperationException("You cannot connect more Warppoints to a WarppointPortal.");
	}
	
	@Override
	public void DisconnectWarppoint(Warppoint warppoint) throws IllegalArgumentException
	{
		if(_first_warppoint == warppoint)
		{
			_first_warppoint = null;
			_is_valid = false;
			return;
		}
		
		if(_second_warppoint == warppoint)
		{
			_second_warppoint = null;
			_is_valid = false;
			return;
		}
		
		throw new IllegalArgumentException("Portal " + toString() + " doesn't include " + warppoint.GetName());
	}
	
	@Override
	public ArrayList<Warppoint> GetAllWarppoints()
	{
		ArrayList<Warppoint> all_wps = new ArrayList<Warppoint>();
		
		if(_first_warppoint != null)
			all_wps.add(_first_warppoint);
		if(_second_warppoint != null)
			all_wps.add(_second_warppoint);
		
		return all_wps;
	}
	
	@Override
	public Warppoint GetDestinationForLocation(final Location location)
	{
		if(_first_warppoint.LocationIsAtWarppoint(location))
			return _second_warppoint;
		
		if(_second_warppoint.LocationIsAtWarppoint(location))
			return _first_warppoint;
		
		return null;
	}
	
	@Override
	public Warppoint GetWarppointByName(final String name)
	{
		if(name.equalsIgnoreCase(_first_warppoint.GetName()))
			return _first_warppoint;
		
		if(name.equalsIgnoreCase(_second_warppoint.GetName()))
			return _second_warppoint;
		
		return null;
	}
	
	@Override
	public boolean IsValid()
	{
		return _is_valid;
	}
	
	@Override
	public boolean NameCollisionWithWarppoints(final String name)
	{
		if(name.equalsIgnoreCase(_first_warppoint.GetName()))
			return true;
		
		if(name.equalsIgnoreCase(_second_warppoint.GetName()))
			return true;
		
		return false;
	}
	
	@Override
	public  String toString()
	{
		return (_first_warppoint.GetName() + " <-> " + _second_warppoint.GetName());
	}
}

package org.bitbucket.tommychenoweth.arkports;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ExecDisconnectWarppoints extends ArkPortsCommandExecutor {

	ExecDisconnectWarppoints(WarppointRepository warppoint_repo)
	{
		super(warppoint_repo);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length != 1)
		{
			sender.sendMessage("Invalid number of arguments.");
			return false;
		}
		
		try
		{
			GetWarppointRepo().DisconnectWarppoint(args[0]);
			sender.sendMessage(args[0] + " was disconnected.");
		}
		catch(Exception e)
		{
			sender.sendMessage(e.getMessage());
			return false;
		}
		
		return true;
    }

}

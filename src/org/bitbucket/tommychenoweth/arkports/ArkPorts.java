package org.bitbucket.tommychenoweth.arkports;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.logging.Level;

import org.bukkit.plugin.java.JavaPlugin;

public class ArkPorts extends JavaPlugin {
	
	private static final String _CMD_LIST_ALL_COMMANDS = "aphelp";
	private static final String _CMD_MAKE_WP = "apwp";
	private static final String _CMD_DELETE_WP = "apdelete";
	private static final String _CMD_MAKE_PORTAL = "apportal";
	private static final String _CMD_MAKE_NEXUS = "apnexus";
	private static final String _CMD_CONNECT_WP = "apconnect";
	private static final String _CMD_DISCONNECT_WP = "apdisc";
	private static final String _CMD_LIST_WP = "aplist";
	
	private static final String _PLUGIN_DIRECTORY = "./plugins/ArkPorts/";
	
	private WarppointRepository _warppoint_repo = new WarppointRepository();
	
	@Override
	public void onEnable()
	{	
		// Every Warppoint needs to be able to access the server to request World objects by name.
		Warppoint.SetServer(getServer());
		
		try
		{
			new File(_PLUGIN_DIRECTORY).mkdir();
			_warppoint_repo.LoadWarppoints(_PLUGIN_DIRECTORY);
		}
		catch(SecurityException e)
		{
			getLogger().log(Level.SEVERE, "Failed to create plugin directory.");
		}
		catch(FileNotFoundException e)
		{
			getLogger().log(Level.WARNING, "Unable to locate saved portals.");
		}
		catch(Exception e)
		{
			getLogger().log(Level.SEVERE, "Exception: " + e.getMessage());
		}
		
		LinkedList<String> all_commands = new LinkedList<String>();
		all_commands.add(_CMD_MAKE_WP);
		all_commands.add(_CMD_DELETE_WP);
		all_commands.add(_CMD_MAKE_PORTAL);
		all_commands.add(_CMD_MAKE_NEXUS);
		all_commands.add(_CMD_CONNECT_WP);
		all_commands.add(_CMD_DISCONNECT_WP);
		all_commands.add(_CMD_LIST_WP);
		
		getCommand(_CMD_LIST_ALL_COMMANDS).setExecutor(new ExecListAllCommands(_warppoint_repo, all_commands));
		getCommand(_CMD_MAKE_WP).setExecutor(new ExecMakeWarppoint(_warppoint_repo));
		getCommand(_CMD_DELETE_WP).setExecutor(new ExecDeleteWarppoint(_warppoint_repo));
		getCommand(_CMD_MAKE_PORTAL).setExecutor(new ExecMakeWarppointPortal(_warppoint_repo));
		getCommand(_CMD_MAKE_NEXUS).setExecutor(new ExecMakeWarppointNexus(_warppoint_repo));
		getCommand(_CMD_CONNECT_WP).setExecutor(new ExecConnectWarppoints(_warppoint_repo));
		getCommand(_CMD_DISCONNECT_WP).setExecutor(new ExecDisconnectWarppoints(_warppoint_repo));
		getCommand(_CMD_LIST_WP).setExecutor(new ExecListWarppoints(_warppoint_repo));
		
		getServer().getPluginManager().registerEvents(new PlayerMoveListener(_warppoint_repo), this);
		
		getLogger().info("ArkPorts is enabled.");
	}
	
	@Override
	public void onDisable()
	{
		try
		{
			_warppoint_repo.SaveWarppoints(_PLUGIN_DIRECTORY);
		}
		catch(Exception e)
		{
			getLogger().log(Level.SEVERE, "Exception: " + e.getMessage());
		}
		getLogger().info("ArkPorts is disabled.");
	}
}
